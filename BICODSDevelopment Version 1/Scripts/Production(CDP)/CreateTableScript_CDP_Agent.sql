USE [DEV_BI_CODS]
GO

/****** Object:  Table [cdp].[Agent]    Script Date: 29-07-2016 15:37:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[Agent](
	[AgentRef] [varchar](255) NULL,
	[AgentName] [varchar](255) NULL,
	[AccountNumber] [varchar](255) NULL,
	[CityName] [varchar](255) NULL,
	[CountryCode] [varchar](255) NULL,
	[StateName] [varchar](255) NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [date] NULL,
	[EventSource] [varchar](255) NULL,
	[EventId] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


