USE [DEV_BI_CODS]
GO

/****** Object:  Table [cdp].[Promotion]    Script Date: 29-07-2016 15:40:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[Promotion](
	[PromotionTrackingCode] [varchar](255) NULL,
	[TargetRegionCode] [varchar](255) NULL,
	[EntryPointCode] [varchar](255) NULL,
	[EntryPointName] [varchar](255) NULL,
	[EntryPointDescription] [varchar](255) NULL,
	[GlobalChannelCode] [varchar](255) NULL,
	[PromotionCode] [varchar](255) NULL,
	[PromotionName] [varchar](255) NULL,
	[PromotionDescription] [varchar](255) NULL,
	[CampaignCode] [varchar](255) NULL,
	[CampaignName] [varchar](255) NULL,
	[CampaignType] [varchar](255) NULL,
	[BusinessFunction] [varchar](255) NULL,
	[CampaignStartDate] [varchar](8) NULL,
	[CampaignEndDate] [varchar](8) NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [date] NULL,
	[EventSource] [varchar](255) NULL,
	[EventId] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


