USE [DEV_BI_CODS]
GO

/****** Object:  Table [cdp].[Subscription]    Script Date: 29-07-2016 15:41:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[Subscription](
	[SubscriptionRef] [varchar](255) NULL,
	[SubscriptionSeqNo] [varchar](255) NULL,
	[ActionDateTime] [datetime] NULL,
	[SubscriptionRoute] [varchar](255) NULL,
	[OrderRoute] [varchar](255) NULL,
	[SubscriptionStatus] [varchar](255) NULL,
	[SubscriberType] [varchar](255) NULL,
	[SubscriberRef] [varchar](255) NULL,
	[AgentRef] [varchar](255) NULL,
	[AgentSubscriptionRef] [varchar](255) NULL,
	[ProductCode] [varchar](255) NULL,
	[OrderDate] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[PricingTerm] [varchar](255) NULL,
	[NoOfIssues] [varchar](255) NULL,
	[PaymentType] [varchar](255) NULL,
	[PaymentMethod] [varchar](255) NULL,
	[CardType] [varchar](255) NULL,
	[PaymentDate] [datetime] NULL,
	[PaymentCcyCode] [varchar](255) NULL,
	[PaymentAmount] [varchar](255) NULL,
	[PaymentCycle] [varchar](255) NULL,
	[DeliveryMethod] [varchar](255) NULL,
	[ContinuousPaymentNo] [varchar](255) NULL,
	[FulfillmentTrackingCode] [varchar](255) NULL,
	[OfferCode] [varchar](255) NULL,
	[RenewalEffortNo] [varchar](255) NULL,
	[RenewalEffortType] [varchar](255) NULL,
	[SubscriptionVolume] [varchar](255) NULL,
	[SubscriptionCcyCode] [varchar](255) NULL,
	[SubscriptionCcyValue] [varchar](255) NULL,
	[SubscriptionCcyTaxValue] [varchar](255) NULL,
	[FeedDate] [date] NULL,
	[EventSource] [varchar](255) NULL,
	[EventId] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL,
	[DeliveryCountry] [varchar](255) NULL,
	[PaymentStatus] [varchar](255) NULL,
	[ExternalSubscriberRef] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


