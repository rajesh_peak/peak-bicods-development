USE [DEV_BI_CODS]
GO

/****** Object:  Table [CEL].[stgPayment]    Script Date: 29-07-2016 15:44:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgPayment](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[OrderNumber] [varchar](255) NULL,
	[OrderSourceSystemId] [varchar](255) NULL,
	[BillingAccountNumber] [varchar](255) NULL,
	[BillingAccountSourceSystemID] [varchar](255) NULL,
	[SubscriptionNumber] [varchar](255) NULL,
	[SubscriptionSourceSystemID] [varchar](255) NULL,
	[PaymentMethod] [varchar](255) NULL,
	[Amount] [varchar](255) NULL,
	[CCTokenNumber] [varchar](255) NULL,
	[CCLEgacyRN] [varchar](255) NULL,
	[CCCardType] [varchar](255) NULL,
	[CCExpiryMonthYear] [varchar](255) NULL,
	[CCLast4Digit] [varchar](255) NULL,
	[CCAddressSourceSystemId] [varchar](255) NULL,
	[DDBankAccountName] [varchar](255) NULL,
	[DDBankAccountCode] [varchar](255) NULL,
	[DDBankSortCode] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[AttemptNumber] [varchar](255) NULL,
	[DDNextDDDate] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


