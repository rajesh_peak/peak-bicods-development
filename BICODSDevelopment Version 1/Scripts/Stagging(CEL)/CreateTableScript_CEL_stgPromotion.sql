USE [DEV_BI_CODS]
GO

/****** Object:  Table [CEL].[stgPromotion]    Script Date: 29-07-2016 15:45:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgPromotion](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[ParentCampaignCode] [varchar](255) NULL,
	[ParentCampaignSourceSystemId] [varchar](255) NULL,
	[PromotionName] [varchar](255) NULL,
	[PromotionCode] [varchar](255) NULL,
	[PromotionBaseURL] [varchar](255) NULL,
	[PromotionProjectManager] [varchar](255) NULL,
	[PromotionType] [varchar](255) NULL,
	[SegmentName] [varchar](255) NULL,
	[SegmentLink] [varchar](255) NULL,
	[PromotionCodeWhenExpired] [varchar](255) NULL,
	[PromotionSourceSystemIDWhenExpired] [varchar](255) NULL,
	[BudgetedCost] [decimal](18, 2) NULL,
	[ActualCost] [decimal](18, 2) NULL,
	[ExpectedResponse] [decimal](18, 2) NULL,
	[ExpectedRevenue] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL,
	[StartSellingDateTime] [varchar](255) NULL,
	[EndSellingDateTime] [varchar](255) NULL,
	[Description] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


