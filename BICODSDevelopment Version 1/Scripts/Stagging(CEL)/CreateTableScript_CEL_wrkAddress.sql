USE [DEV_BI_CODS]
GO

/****** Object:  Table [CEL].[wrkAddress]    Script Date: 29-07-2016 15:49:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[wrkAddress](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[AccountSourceSystemID] [varchar](255) NULL,
	[AccountNumber] [varchar](255) NULL,
	[CompanyName] [varchar](255) NULL,
	[AddressLine1] [varchar](255) NULL,
	[AddressLine2] [varchar](255) NULL,
	[AddressLine3] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [varchar](255) NULL,
	[ZipCode] [varchar](255) NULL,
	[CountryISO3Code] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[MobilePhone] [varchar](255) NULL,
	[IsPrimaryContact] [varchar](255) NULL,
	[IsDefaultBillingAddress] [bit] NULL,
	[IsDefaultShippingAddress] [bit] NULL,
	[Status] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [datetime] NULL,
	[EffectiveStartDate] [datetime] NULL,
	[EffectiveEndDate] [datetime] NULL,
	[RecordType] [varchar](255) NULL,
	[DeliveryInstruction] [varchar](255) NULL,
	[StartDt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


