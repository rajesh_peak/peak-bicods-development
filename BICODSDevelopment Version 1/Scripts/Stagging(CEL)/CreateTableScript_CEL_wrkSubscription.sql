USE [DEV_BI_CODS]
GO

/****** Object:  Table [CEL].[wrkSubscription]    Script Date: 29-07-2016 15:52:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[wrkSubscription](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[OrderNumber] [varchar](255) NULL,
	[SubscriptionNumber] [varchar](255) NULL,
	[SubscriptionRenewalSequenceNo] [varchar](255) NULL,
	[SubscriberAccountNumber] [varchar](255) NULL,
	[SubscriberAccountSourceSystemID] [varchar](255) NULL,
	[BillingAccountSourceSystemID] [varchar](255) NULL,
	[BillingAccountNumber] [varchar](255) NULL,
	[BillingAddressSourceSystemId] [varchar](255) NULL,
	[DeliveryAddressSourceSystemID] [varchar](255) NULL,
	[LegacyReference] [varchar](255) NULL,
	[OrderRoute] [varchar](255) NULL,
	[SubscriptionStatus] [varchar](255) NULL,
	[PaymentStatus] [varchar](255) NULL,
	[OrderDate] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[PaymentDate] [datetime] NULL,
	[NoOfCopies] [varchar](255) NULL,
	[NoofIssues] [varchar](255) NULL,
	[TermType] [varchar](255) NULL,
	[TermLength] [varchar](255) NULL,
	[OfferItemSourceSystemID] [varchar](255) NULL,
	[OfferItemCode] [varchar](255) NULL,
	[LegacyProductBundleCode] [varchar](255) NULL,
	[PromotionCode] [varchar](255) NULL,
	[CampaignCode] [varchar](255) NULL,
	[OfferCode] [varchar](255) NULL,
	[LegacyRateCardCode] [varchar](255) NULL,
	[BillingCurrencyISOCode] [varchar](255) NULL,
	[BaseCurrencyISOCode] [varchar](255) NULL,
	[EntryPointSourceSystemId] [varchar](255) NULL,
	[EntryPointCode] [varchar](255) NULL,
	[AgentPromoCode] [varchar](255) NULL,
	[ValueBillingCurr] [varchar](255) NULL,
	[ValueBaseCurr] [varchar](255) NULL,
	[PaidAmountBillingCurr] [varchar](255) NULL,
	[PaidAmountBaseCurr] [varchar](255) NULL,
	[BilltoBaseExhRate] [varchar](255) NULL,
	[TaxValueBillingCurr] [varchar](255) NULL,
	[TaxValueBaseCurr] [varchar](255) NULL,
	[CommValueBillCur] [varchar](255) NULL,
	[CommValueBaseCur] [varchar](255) NULL,
	[AgentPaymentMode] [varchar](255) NULL,
	[RenewalMethod] [varchar](255) NULL,
	[CreditIssues] [varchar](255) NULL,
	[FollowUpTermType] [varchar](255) NULL,
	[FollowUpTermLength] [varchar](255) NULL,
	[FollowUpOfferPriceBillingCurr] [varchar](255) NULL,
	[RenewalEffortNo] [varchar](255) NULL,
	[CancelOnExpiry] [varchar](255) NULL,
	[CancellationDate] [datetime] NULL,
	[LastIssueSent] [varchar](255) NULL,
	[IssuesServed] [varchar](255) NULL,
	[IssuesPending] [varchar](255) NULL,
	[OfferType] [varchar](255) NULL,
	[DonorType] [varchar](255) NULL,
	[InvoiceEffortCycle] [varchar](255) NULL,
	[LegacyComments] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL,
	[OfferRateType] [varchar](255) NULL,
	[ABCRateBand] [varchar](255) NULL,
	[ABCRateBandDigital] [varchar](255) NULL,
	[ABCClaimType] [varchar](255) NULL,
	[NewStandPriceBillCurr] [varchar](255) NULL,
	[BarRateBillCurr] [varchar](255) NULL,
	[DeliveryMethod] [varchar](255) NULL,
	[StartDt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


