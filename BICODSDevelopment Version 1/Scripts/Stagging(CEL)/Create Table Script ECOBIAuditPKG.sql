USE [DEV_BI_CODS]
GO

/****** Object:  Table [cel].[ECOBIAuditPKG]    Script Date: 11/07/2016 14:36:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cel].[ECOBIAuditPKG](
	[PKGName] [varchar](50) NULL,
	[DestRowCount] [int] NULL,
	[StartTime] [datetimeoffset](7) NULL,
	[EndTime] [datetimeoffset](7) NULL,
	[ExecutionTime] [int] NULL,
	[UserName] [varchar](50) NULL,
	[pkgstatus] [varchar](30) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


