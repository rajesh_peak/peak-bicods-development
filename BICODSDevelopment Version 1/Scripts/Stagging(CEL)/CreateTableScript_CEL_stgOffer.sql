USE [DEV_BI_CODS]
GO

/****** Object:  Table [CEL].[stgOffer]    Script Date: 29-07-2016 15:44:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgOffer](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[OfferType] [varchar](255) NULL,
	[OfferHeaderCode] [varchar](255) NULL,
	[OfferHeaderName] [varchar](255) NULL,
	[OfferRateType] [varchar](255) NULL,
	[ParentPromotionCode] [varchar](255) NULL,
	[ParentPromotionSourceSystemId] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


