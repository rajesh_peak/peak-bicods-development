--USE [DEV_BI_CODS]

GO

/****** Object:  Index [idx_NonUnique_ECOBIAuditPKG_PKGStatus_PKGName]    Script Date: 11/21/2016 1:12:16 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_ECOBIAuditPKG_PKGStatus_PKGName] ON [cdp].[ECOBIAuditPKG]
(
	[pkgstatus] ASC,
	[PKGName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_offer_offercode]    Script Date: 11/21/2016 1:09:14 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_offer_offercode] ON [cdp].[Offer]
(
	[OfferCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_Promotion_CampaignCode]    Script Date: 11/21/2016 12:47:12 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_Promotion_CampaignCode] ON [cdp].[Promotion]
(
	[CampaignCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_Promotion_EntryPointCode]    Script Date: 11/21/2016 12:47:39 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_Promotion_EntryPointCode] ON [cdp].[Promotion]
(
	[EntryPointCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_Promotion_PromotionCode]    Script Date: 11/21/2016 12:48:02 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_Promotion_PromotionCode] ON [cdp].[Promotion]
(
	[PromotionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_stgCampaign_eventdatetime]    Script Date: 11/21/2016 2:15:03 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_stgCampaign_eventdatetime] ON [CEL].[stgCampaign]
(
	[EventDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_stgCustomerFC_eventdatetime]    Script Date: 11/21/2016 2:14:21 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_stgCustomerFC_eventdatetime] ON [CEL].[stgCustomerFC]
(
	[EventDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_stgEntryPoint_eventdatetime]    Script Date: 11/21/2016 2:13:37 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_stgEntryPoint_eventdatetime] ON [CEL].[stgEntryPoint]
(
	[EventDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_stgOffer_eventdatetime]    Script Date: 11/21/2016 2:12:46 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_stgOffer_eventdatetime] ON [CEL].[stgOffer]
(
	[EventDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_stgPromotion_eventdatetime]    Script Date: 11/21/2016 2:12:03 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_stgPromotion_eventdatetime] ON [CEL].[stgPromotion]
(
	[EventDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkCampaign_campaigncode]    Script Date: 11/18/2016 6:47:12 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkCampaign_campaigncode] ON [CEL].[wrkCampaign]
(
	[CampaignCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkCustomerFC_Code_PromotionCode_EntryPointPromoTC]    Script Date: 11/18/2016 6:27:59 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkCustomerFC_Code_PromotionCode_EntryPointPromoTC] ON [CEL].[wrkCustomerFC]
(
	[Code] ASC,
	[PromotionCode] ASC,
	[EntryPointPromoTrackingCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkCustomerFC_EntryPointPromoTrackingCode]    Script Date: 11/21/2016 12:39:48 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkCustomerFC_EntryPointPromoTrackingCode] ON [CEL].[wrkCustomerFC]
(
	[EntryPointPromoTrackingCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkCustomerFC_PromotionCode]    Script Date: 11/21/2016 12:40:12 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkCustomerFC_PromotionCode] ON [CEL].[wrkCustomerFC]
(
	[PromotionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkentrypoint_EntryPointTrackingCode]    Script Date: 11/21/2016 12:38:46 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkentrypoint_EntryPointTrackingCode] ON [CEL].[wrkEntryPoint]
(
	[EntryPointTrackingCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkEntryPoint_EntryPointTrackingCode_PromotionCode]    Script Date: 11/18/2016 6:19:43 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkEntryPoint_EntryPointTrackingCode_PromotionCode] ON [CEL].[wrkEntryPoint]
(
	[EntryPointTrackingCode] ASC,
	[PromotionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkentrypoint_ParentCampaignCode]    Script Date: 11/21/2016 12:39:09 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkentrypoint_ParentCampaignCode] ON [CEL].[wrkEntryPoint]
(
	[PromotionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkOffer_OfferHeaderCode]    Script Date: 11/18/2016 6:10:41 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkOffer_OfferHeaderCode] ON [CEL].[wrkOffer]
(
	[OfferHeaderCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkPromotion_ParentCampaignCode]    Script Date: 11/21/2016 12:27:47 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkPromotion_ParentCampaignCode] ON [CEL].[wrkPromotion]
(
	[ParentCampaignCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



GO

/****** Object:  Index [idx_NonUnique_wrkPromotion_PromotionCode]    Script Date: 11/18/2016 6:05:59 PM ******/
CREATE NONCLUSTERED INDEX [idx_NonUnique_wrkPromotion_PromotionCode] ON [CEL].[wrkPromotion]
(
	[PromotionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO