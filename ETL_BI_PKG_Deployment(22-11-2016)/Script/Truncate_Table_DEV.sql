--USE [DEV_BI_CODS]

GO

truncate table [cdp].[ECOBIAuditPKG]
truncate table [cdp].[ErrLogging]
truncate table [cdp].[ErrOfferRows]
truncate table [cdp].[ErrPromotionRec]
truncate table [cdp].[Offer]
truncate table [cdp].[Promotion]
truncate table [CEL].[stgCampaign]
truncate table [CEL].[stgCustomerFC]
truncate table [CEL].[stgEntryPoint]
truncate table [CEL].[stgOffer]
truncate table [CEL].[stgPromotion]
truncate table [CEL].[wrkCampaign]
truncate table [CEL].[wrkCustomerFC]
truncate table [CEL].[wrkEntryPoint]
truncate table [CEL].[wrkOffer]
truncate table [CEL].[wrkPromotion]


GO