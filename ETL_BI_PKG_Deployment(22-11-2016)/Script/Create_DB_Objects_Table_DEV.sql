--USE [DEV_BI_CODS]
GO

/****** Object:  Table [cdp].[ECOBIAuditPKG]    Script Date: 11/18/2016 5:18:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[ECOBIAuditPKG](
	[PKGName] [varchar](50) NULL,
	[DestRowCount] [int] NULL,
	[StartTime] [datetimeoffset](7) NULL,
	[EndTime] [datetimeoffset](7) NULL,
	[ExecutionTime] [int] NULL,
	[UserName] [varchar](50) NULL,
	[pkgstatus] [varchar](30) NULL,
	[Master_Pkg_StartTime] [datetime] NULL,
	[ErrCount] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [cdp].[ECOBIAuditPKG] ADD  DEFAULT ((0)) FOR [ErrCount]
GO



GO

/****** Object:  Table [cdp].[ErrLogging]    Script Date: 11/18/2016 5:20:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[ErrLogging](
	[PKGName] [varchar](50) NULL,
	[TaskName] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[ErrEventTime] [datetime] NULL
) 

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [cdp].[ErrOfferRows]    Script Date: 11/21/2016 3:38:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[ErrOfferRows](
	[OfferHeaderCode] [varchar](20) NULL,
	[OfferHeaderName] [varchar](80) NULL,
	[OfferType] [varchar](30) NULL,
	[OfferRateType] [varchar](30) NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](80) NULL,
	[EventSource] [varchar](5) NULL,
	[EventId] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [cdp].[ErrPromotionRec]    Script Date: 11/21/2016 3:38:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[ErrPromotionRec](
	[EntrypointTrackingCode] [varchar](20) NULL,
	[TargetGeographicalArea] [varchar](255) NULL,
	[EntryPointCode] [varchar](20) NULL,
	[EntryPointName] [varchar](80) NULL,
	[EntryPointDescription] [int] NULL,
	[GlobalChannelCode] [varchar](255) NULL,
	[ListQuantity] [int] NULL,
	[ListType] [varchar](255) NULL,
	[ListName] [varchar](80) NULL,
	[PromotionCode] [varchar](20) NULL,
	[PromotionName] [varchar](80) NULL,
	[PromotionDescription] [varchar](255) NULL,
	[CampaignCode] [varchar](255) NULL,
	[CampaignName] [varchar](255) NULL,
	[CampaignType] [varchar](255) NULL,
	[BusinessFunction] [varchar](255) NULL,
	[CampaignStartDate] [datetime] NULL,
	[CampaignEndDate] [datetime] NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [datetime] NULL,
	[CreatedBy] [varchar](2) NULL,
	[CreateDate] [datetime] NULL,
	[EventSource] [varchar](10) NULL,
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[SegmentName] [varchar](80) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[stgCampaign]    Script Date: 11/18/2016 5:32:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgCampaign](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[CampaignName] [varchar](255) NULL,
	[CampaignDescription] [varchar](1000) NULL,
	[CampaignCode] [varchar](255) NULL,
	[CampaignRegion] [varchar](255) NULL,
	[CampaignType] [varchar](255) NULL,
	[CampaignBusinessFunction] [varchar](255) NULL,
	[CampaignBudgetedCost] [decimal](18, 2) NULL,
	[CampaignActualCost] [decimal](18, 2) NULL,
	[CampaignExpectedResponse] [decimal](18, 2) NULL,
	[CampaignExpectedRevenue] [decimal](18, 2) NULL,
	[CampaignStartDate] [datetime] NULL,
	[CampaignEndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](255) NULL,
	[BaseURL] [varchar](255) NULL,
	[FolderName] [varchar](255) NULL,
	[IsDefaultCampaign] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[stgCustomerFC]    Script Date: 11/18/2016 5:34:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgCustomerFC](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[Type] [varchar](30) NULL,
	[Region] [varchar](80) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[PromotionCode] [varchar](20) NOT NULL,
	[PromotionSourceSystemId] [varchar](20) NOT NULL,
	[PromotionSourceSystem] [varchar](80) NULL,
	[EntryPointSourceSystemId] [varchar](20) NULL,
	[EntryPointPromoTrackingCode] [varchar](20) NULL,
	[CanonicalId] [varchar](255) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[stgEntryPoint]    Script Date: 11/18/2016 5:34:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgEntryPoint](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[SourceSystemId] [varchar](20) NOT NULL,
	[Name] [varchar](80) NOT NULL,
	[EntryPointTrackingCode] [varchar](20) NOT NULL,
	[Status] [varchar](30) NOT NULL,
	[MarketingMediaChannel] [varchar](255) NOT NULL,
	[ECOModelSourceOATRegular] [varchar](255) NULL,
	[ECOModelSourceOATStudent] [varchar](255) NULL,
	[ECOModelSourceOATGift] [varchar](255) NULL,
	[ECAModelSourceOATRegular] [varchar](255) NULL,
	[ECAModelSourceOATStudent] [varchar](255) NULL,
	[ECAModelSourceOATGift] [varchar](255) NULL,
	[ECUModelSourceOATRegular] [varchar](255) NULL,
	[ECUModelSourceOATStudent] [varchar](255) NULL,
	[ECUModelSourceOATGift] [varchar](255) NULL,
	[BillMeCreditCopies] [int] NULL,
	[ListQuantity] [int] NULL,
	[ListType] [varchar](255) NULL,
	[ListName] [varchar](80) NULL,
	[PromotionCode] [varchar](20) NOT NULL,
	[PromotionSourceSystemId] [varchar](20) NOT NULL,
	[PromotionSourceSystem] [varchar](30) NULL,
	[CanonicalId] [varchar](255) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[stgOffer]    Script Date: 11/18/2016 5:35:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgOffer](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[SourceSystem] [varchar](80) NOT NULL,
	[SourceSystemId] [varchar](20) NOT NULL,
	[CanonicalId] [varchar](255) NOT NULL,
	[OfferType] [varchar](30) NOT NULL,
	[OfferHeaderCode] [varchar](20) NOT NULL,
	[OfferHeaderName] [varchar](80) NULL,
	[OfferRateType] [varchar](30) NOT NULL,
	[ParentPromotionCode] [varchar](20) NULL,
	[ParentPromotionSourceSystemId] [varchar](20) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](80) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[stgPromotion]    Script Date: 11/18/2016 5:36:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[stgPromotion](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[SourceSystem] [varchar](80) NOT NULL,
	[SourceSystemId] [varchar](20) NOT NULL,
	[CanonicalId] [varchar](255) NOT NULL,
	[ParentCampaignCode] [varchar](20) NOT NULL,
	[ParentCampaignSourceSystemId] [varchar](20) NULL,
	[PromotionName] [varchar](80) NOT NULL,
	[PromotionCode] [varchar](20) NOT NULL,
	[PromotionBaseURL] [varchar](255) NULL,
	[PromotionProjectManager] [varchar](80) NULL,
	[PromotionType] [varchar](30) NOT NULL,
	[SegmentName] [varchar](80) NULL,
	[SegmentLink] [varchar](255) NULL,
	[PromotionCodeWhenExpired] [varchar](255) NULL,
	[PromotionSourceSystemIDWhenExpired] [varchar](20) NULL,
	[BudgetedCost] [decimal](18, 2) NULL,
	[ActualCost] [decimal](18, 2) NULL,
	[ExpectedResponse] [decimal](18, 2) NULL,
	[ExpectedRevenue] [decimal](18, 2) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[StartSellingDateTime] [datetime] NOT NULL,
	[EndSellingDateTime] [datetime] NULL,
	[Description] [varchar](255) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[wrkCampaign]    Script Date: 11/18/2016 5:36:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[wrkCampaign](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[CampaignName] [varchar](255) NULL,
	[CampaignDescription] [varchar](1000) NULL,
	[CampaignCode] [varchar](255) NULL,
	[CampaignRegion] [varchar](255) NULL,
	[CampaignType] [varchar](255) NULL,
	[CampaignBusinessFunction] [varchar](255) NULL,
	[CampaignBudgetedCost] [decimal](18, 2) NULL,
	[CampaignActualCost] [decimal](18, 2) NULL,
	[CampaignExpectedResponse] [decimal](18, 2) NULL,
	[CampaignExpectedRevenue] [decimal](18, 2) NULL,
	[CampaignStartDate] [datetime] NULL,
	[CampaignEndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](255) NULL,
	[BaseURL] [varchar](255) NULL,
	[FolderName] [varchar](255) NULL,
	[IsDefaultCampaign] [bit] NULL,
	[StartDt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[wrkCustomerFC]    Script Date: 11/18/2016 5:37:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[wrkCustomerFC](
	[EventID] [bigint] NOT NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[Type] [varchar](30) NULL,
	[Region] [varchar](80) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[PromotionCode] [varchar](20) NOT NULL,
	[PromotionSourceSystemId] [varchar](20) NOT NULL,
	[PromotionSourceSystem] [varchar](80) NULL,
	[EntryPointSourceSystemId] [varchar](20) NULL,
	[EntryPointPromoTrackingCode] [varchar](20) NULL,
	[CanonicalId] [varchar](255) NOT NULL,
	[StartDt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[wrkEntryPoint]    Script Date: 11/18/2016 5:41:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[wrkEntryPoint](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[SourceSystemId] [varchar](20) NOT NULL,
	[Name] [varchar](80) NOT NULL,
	[EntryPointTrackingCode] [varchar](20) NOT NULL,
	[Status] [varchar](30) NOT NULL,
	[MarketingMediaChannel] [varchar](255) NOT NULL,
	[ECOModelSourceOATRegular] [varchar](255) NULL,
	[ECOModelSourceOATStudent] [varchar](255) NULL,
	[ECOModelSourceOATGift] [varchar](255) NULL,
	[ECAModelSourceOATRegular] [varchar](255) NULL,
	[ECAModelSourceOATStudent] [varchar](255) NULL,
	[ECAModelSourceOATGift] [varchar](255) NULL,
	[ECUModelSourceOATRegular] [varchar](255) NULL,
	[ECUModelSourceOATStudent] [varchar](255) NULL,
	[ECUModelSourceOATGift] [varchar](255) NULL,
	[BillMeCreditCopies] [int] NULL,
	[ListQuantity] [int] NULL,
	[ListType] [varchar](255) NULL,
	[ListName] [varchar](80) NULL,
	[PromotionCode] [varchar](255) NOT NULL,
	[PromotionSourceSystemId] [varchar](255) NOT NULL,
	[PromotionSourceSystem] [varchar](30) NULL,
	[CanonicalId] [varchar](255) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[Startdt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[wrkOffer]    Script Date: 11/18/2016 5:41:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[wrkOffer](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[SourceSystem] [varchar](80) NOT NULL,
	[SourceSystemId] [varchar](20) NOT NULL,
	[CanonicalId] [varchar](255) NOT NULL,
	[OfferType] [varchar](30) NOT NULL,
	[OfferHeaderCode] [varchar](20) NOT NULL,
	[OfferHeaderName] [varchar](80) NULL,
	[OfferRateType] [varchar](30) NOT NULL,
	[ParentPromotionCode] [varchar](20) NULL,
	[ParentPromotionSourceSystemId] [varchar](20) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](255) NULL,
	[StartDt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [CEL].[wrkPromotion]    Script Date: 11/18/2016 5:42:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [CEL].[wrkPromotion](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[SourceSystem] [varchar](80) NOT NULL,
	[SourceSystemId] [varchar](20) NOT NULL,
	[CanonicalId] [varchar](255) NOT NULL,
	[ParentCampaignCode] [varchar](20) NOT NULL,
	[ParentCampaignSourceSystemId] [varchar](20) NULL,
	[PromotionName] [varchar](80) NOT NULL,
	[PromotionCode] [varchar](20) NOT NULL,
	[PromotionBaseURL] [varchar](255) NULL,
	[PromotionProjectManager] [varchar](80) NULL,
	[PromotionType] [varchar](30) NOT NULL,
	[SegmentName] [varchar](80) NULL,
	[SegmentLink] [varchar](255) NULL,
	[PromotionCodeWhenExpired] [varchar](255) NULL,
	[PromotionSourceSystemIDWhenExpired] [varchar](20) NULL,
	[BudgetedCost] [decimal](18, 2) NULL,
	[ActualCost] [decimal](18, 2) NULL,
	[ExpectedResponse] [decimal](18, 2) NULL,
	[ExpectedRevenue] [decimal](18, 2) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](255) NULL,
	[StartSellingDateTime] [datetime] NOT NULL,
	[EndSellingDateTime] [datetime] NULL,
	[Description] [varchar](255) NULL,
	[StartDT] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO

/****** Object:  Table [cdp].[Offer]    Script Date: 11/21/2016 3:37:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[Offer](
	[OfferCode] [varchar](255) NULL,
	[OfferName] [varchar](255) NULL,
	[OfferType] [varchar](255) NULL,
	[RateType] [varchar](255) NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [date] NULL,
	[CreatedBy] [varchar](80) NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](80) NULL,
	[EventSource] [varchar](255) NULL,
	[EventId] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL,
	[RowDataSourceRecordGUID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [cdp].[Offer] ADD  CONSTRAINT [DF_cdp_Offer_RowDataSourceRecordGUID]  DEFAULT (newsequentialid()) FOR [RowDataSourceRecordGUID]
GO



GO

/****** Object:  Table [cdp].[Promotion]    Script Date: 11/21/2016 3:33:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[Promotion](
	[EntrypointTrackingCode] [varchar](20) NOT NULL,
	[TargetGeographicalArea] [varchar](20) NULL,
	[EntryPointCode] [varchar](20) NOT NULL,
	[EntryPointName] [varchar](80) NOT NULL,
	[EntryPointDescription] [varchar](255) NULL,
	[GlobalChannelCode] [varchar](200) NULL,
	[ListQuantity] [int] NOT NULL,
	[ListType] [varchar](30) NOT NULL,
	[ListName] [varchar](80) NOT NULL,
	[PromotionCode] [varchar](20) NOT NULL,
	[PromotionName] [varchar](80) NOT NULL,
	[PromotionDescription] [varchar](255) NULL,
	[CampaignCode] [varchar](20) NOT NULL,
	[CampaignName] [varchar](80) NOT NULL,
	[CampaignType] [varchar](30) NOT NULL,
	[BusinessFunction] [varchar](255) NULL,
	[CampaignStartDate] [date] NOT NULL,
	[CampaignEndDate] [date] NOT NULL,
	[ActionDateTime] [datetime] NOT NULL,
	[FeedDate] [date] NOT NULL,
	[CreatedBy] [varchar](80) NULL,
	[CreateDate] [datetime] NULL,
	[EventSource] [varchar](80) NOT NULL,
	[EventId] [bigint] NOT NULL,
	[EventName] [varchar](80) NOT NULL,
	[BIStatus] [varchar](20) NOT NULL,
	[CDWStatus] [varchar](20) NOT NULL,
	[SegmentName] [varchar](80) NULL,
	[RowDataSourceRecordGUID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [cdp].[Promotion] ADD  CONSTRAINT [DF_cdp_Promotion_RowDataSourceRecordGUID]  DEFAULT (newsequentialid()) FOR [RowDataSourceRecordGUID]
GO
