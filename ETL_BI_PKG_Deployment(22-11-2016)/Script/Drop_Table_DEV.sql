--USE [DEV_BI_CODS]

GO

drop table [cdp].[ECOBIAuditPKG]
drop table [cdp].[ErrLogging]
drop table [cdp].[ErrOfferRows]
drop table [cdp].[ErrPromotionRec]
drop table [cdp].[Offer]
drop table [cdp].[Promotion]
drop table [CEL].[stgCampaign]
drop table [CEL].[stgCustomerFC]
drop table [CEL].[stgEntryPoint]
drop table [CEL].[stgOffer]
drop table [CEL].[stgPromotion]
drop table [CEL].[wrkCampaign]
drop table [CEL].[wrkCustomerFC]
drop table [CEL].[wrkEntryPoint]
drop table [CEL].[wrkOffer]
drop table [CEL].[wrkPromotion]


GO