USE [DEV_BI_CODS]
GO

/****** Object:  Table [cdp].[ErrPromotionRec]    Script Date: 28-10-2016 18:33:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[ErrPromotionRec](
	[EntrypointTrackingCode] [varchar](20) NULL,
	[TargetRegionCode] [varchar](255) NULL,
	[EntryPointCode] [varchar](20) NULL,
	[EntryPointName] [varchar](80) NULL,
	[EntryPointDescription] [int] NULL,
	[GlobalChannelCode] [varchar](255) NULL,
	[ListQuantity] [int] NULL,
	[ListType] [varchar](255) NULL,
	[ListName] [varchar](80) NULL,
	[PromotionCode] [varchar](20) NULL,
	[PromotionName] [varchar](80) NULL,
	[PromotionDescription] [varchar](255) NULL,
	[CampaignCode] [varchar](255) NULL,
	[CampaignName] [varchar](255) NULL,
	[CampaignType] [varchar](255) NULL,
	[BusinessFunction] [varchar](255) NULL,
	[CampaignStartDate] [datetime] NULL,
	[CampaignEndDate] [datetime] NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [datetime] NULL,
	[CreatedBy] [varchar](2) NULL,
	[CreateDate] [datetime] NULL,
	[EventSource] [varchar](10) NULL,
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO





USE [DEV_BI_CODS]
GO

/****** Object:  Table [cdp].[ErrOfferRows]    Script Date: 28-10-2016 18:34:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[ErrOfferRows](
	[OfferHeaderCode] [varchar](20) NULL,
	[OfferHeaderName] [varchar](80) NULL,
	[OfferType] [varchar](30) NULL,
	[OfferRateType] [varchar](30) NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](80) NULL,
	[EventSource] [varchar](5) NULL,
	[EventId] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO






