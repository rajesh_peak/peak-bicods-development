USE [DEV_BI_CODS]
GO

/****** Object:  Table [cdp].[Offer]    Script Date: 29-07-2016 15:39:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cdp].[Offer](
	[OfferCode] [varchar](255) NULL,
	[OfferName] [varchar](255) NULL,
	[OfferType] [varchar](255) NULL,
	[RateType] [varchar](255) NULL,
	[ActionDateTime] [datetime] NULL,
	[FeedDate] [varchar](8) NULL,
	[EventSource] [varchar](255) NULL,
	[EventId] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[BIStatus] [varchar](1) NULL,
	[CDWStatus] [varchar](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


