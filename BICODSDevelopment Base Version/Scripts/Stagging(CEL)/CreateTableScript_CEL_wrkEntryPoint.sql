USE [DEV_BI_CODS]
GO

/****** Object:  Table [cel].[wrkEntryPoint]    Script Date: 29-07-2016 15:50:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cel].[wrkEntryPoint](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystemId] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[PromoTrackingCode] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[MarketingMediaChannel] [varchar](255) NULL,
	[ECOModelSourceOATRegular] [varchar](255) NULL,
	[ECOModelSourceOATStudent] [varchar](255) NULL,
	[ECOModelSourceOATGift] [varchar](255) NULL,
	[ECAModelSourceOATRegular] [varchar](255) NULL,
	[ECAModelSourceOATStudent] [varchar](255) NULL,
	[ECAModelSourceOATGift] [varchar](255) NULL,
	[ECUModelSourceOATRegular] [varchar](255) NULL,
	[ECUModelSourceOATStudent] [varchar](255) NULL,
	[ECUModelSourceOATGift] [varchar](255) NULL,
	[BillMeCreditCopies] [int] NULL,
	[PromotionCode] [varchar](255) NULL,
	[PromotionSourceSystemId] [varchar](255) NULL,
	[PromotionSourceSystem] [varchar](255) NULL,
	[BIEventId] [int] NULL,
	[CanonicalId] [varchar](255) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[Startdt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


