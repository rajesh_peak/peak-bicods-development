USE [DEV_BI_CODS]
GO

/****** Object:  Table [cel].[wrkAccount]    Script Date: 29-07-2016 15:48:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cel].[wrkAccount](
	[EventID] [bigint] NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[LegacyCRN] [varchar](255) NULL,
	[LegacyReaderCode] [varchar](255) NULL,
	[AccountNumber] [varchar](255) NULL,
	[DrupalUserID] [varchar](255) NULL,
	[ContactEmail] [varchar](255) NULL,
	[PasswordHashValue] [varchar](255) NULL,
	[AccountName] [varchar](255) NULL,
	[Title] [varchar](255) NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[PenName] [varchar](255) NULL,
	[CountryISO3Code] [varchar](255) NULL,
	[BirthYear] [varchar](255) NULL,
	[Gender] [varchar](255) NULL,
	[CreatedDate] [varchar](255) NULL,
	[CreatedBy] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[JobTitle] [varchar](255) NULL,
	[Industry] [varchar](255) NULL,
	[IndustrySICCode] [varchar](255) NULL,
	[CommentNotificationFlag] [varchar](255) NULL,
	[ContactAllowedEmailEconomist] [varchar](255) NULL,
	[ContactAllowedEmailEIU] [varchar](255) NULL,
	[ContactAllowedEmailOthers] [varchar](255) NULL,
	[UserType] [varchar](255) NULL,
	[AccountType] [varchar](255) NULL,
	[UserRegistrationDate] [varchar](255) NULL,
	[UserConversionDate] [varchar](255) NULL,
	[CompanyVatNumber] [varchar](255) NULL,
	[CompanyRegistrationNumber] [varchar](255) NULL,
	[AcquistionChannel] [varchar](255) NULL,
	[UserRegistrationOrigin] [varchar](255) NULL,
	[NewsLetter] [varchar](255) NULL,
	[NewsLetterVersion] [varchar](255) NULL,
	[RegistrationEmail] [varchar](255) NULL,
	[Department] [varchar](255) NULL,
	[IsCustomerVATExampt] [varchar](255) NULL,
	[StartDt] [varchar](8) NULL,
	[StartTime] [varchar](12) NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


