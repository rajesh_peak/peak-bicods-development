USE [DEV_BI_CODS]
GO

/****** Object:  Table [cel].[stgCampaign]    Script Date: 29-07-2016 15:43:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [cel].[stgCampaign](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventName] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[SourceSystem] [varchar](255) NULL,
	[SourceSystemId] [varchar](255) NULL,
	[CanonicalId] [varchar](255) NULL,
	[CampaignName] [varchar](255) NULL,
	[CampaignDescription] [varchar](1000) NULL,
	[CampaignCode] [varchar](255) NULL,
	[CampaignRegion] [varchar](255) NULL,
	[CampaignType] [varchar](255) NULL,
	[CampaignBusinessFunction] [varchar](255) NULL,
	[CampaignBudgetedCost] [decimal](18, 2) NULL,
	[CampaignActualCost] [decimal](18, 2) NULL,
	[CampaignExpectedResponse] [decimal](18, 2) NULL,
	[CampaignExpectedRevenue] [decimal](18, 2) NULL,
	[CampaignStartDate] [datetime] NULL,
	[CampaignEndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NULL,
	[BaseURL] [varchar](255) NULL,
	[FolderName] [varchar](255) NULL,
	[IsDefaultCampaign] [bit] NULL,
	[BIEventId] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


